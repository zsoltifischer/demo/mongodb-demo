package hu.zsfischer.mongodb.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("groceryitems")
public class GroceryItem {

    //Specify the MongoDB document’s primary key _id using the @Id annotation.
    // If we don’t specify anything, MongoDB will generate an _id field while creating the document.
    @Id
    private String id;
    private String name;
    private int quantity;
    private String category;
    private boolean active;

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(id).append(", ");
        sb.append("Name: ").append(name).append(", ");
        sb.append("Quantity: ").append(quantity).append(", ");
        sb.append("Category: ").append(category).append(", ");
        sb.append("Active: ").append(active);
        return sb.toString();
    }
}
