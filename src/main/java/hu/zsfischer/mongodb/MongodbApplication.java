package hu.zsfischer.mongodb;

import hu.zsfischer.mongodb.model.GroceryItem;
import hu.zsfischer.mongodb.repository.GroceryItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;

@SpringBootApplication
public class MongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongodbApplication.class, args);
	}

	@Bean
	CommandLineRunner run(GroceryItemRepository groceryItemRepository) {
		return args -> {
			groceryItemRepository.save(new GroceryItem("Whole Wheat Biscuit", "Whole Wheat Biscuit", 5, "snacks", true));
			groceryItemRepository.save(new GroceryItem("Kodo Millet", "XYZ Kodo Millet healthy", 2, "millets", false));
			groceryItemRepository.save(new GroceryItem("Dried Red Chilli", "Dried Whole Red Chilli", 2, "spices", false));
			groceryItemRepository.save(new GroceryItem("Pearl Millet", "Healthy Pearl Millet", 1, "millets", false));
			groceryItemRepository.save(new GroceryItem("Cheese Crackers", "Bonny Cheese Crackers Plain", 6, "snacks", true));

			System.out.println("----------------FIND BY NAME----------------");
			System.out.println(groceryItemRepository.findItemByName("Healthy Pearl Millet"));
			System.out.println();

			System.out.println("----------------FIND PAGE BY QUANTITY----------------");
			groceryItemRepository.findPageByQuantity(2, PageRequest.of(0, 1)).getContent().forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY CATEGORY----------------");
			groceryItemRepository.findByCategory("snacks").forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY QUANTITY GREATER THAN----------------");
			groceryItemRepository.findByQuantityGreaterThan(5).forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY QUANTITY GREATER THAN EQUAL----------------");
			groceryItemRepository.findByQuantityGreaterThanEqual(5).forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY QUANTITY LESS THAN----------------");
			groceryItemRepository.findByQuantityLessThan(2).forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY QUANTITY LESS THAN EQUAL----------------");
			groceryItemRepository.findByQuantityLessThanEqual(2).forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY QUANTITY BETWEEN----------------");
			groceryItemRepository.findByQuantityBetween(4, 6).forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY NAME IS NULL----------------");
			groceryItemRepository.findByNameNull().forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY NAME IS NOT NULL----------------");
			groceryItemRepository.findByNameNotNull().forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY ACTIVE----------------");
			groceryItemRepository.findByActive().forEach(System.out::println);
			System.out.println();

			System.out.println("----------------FIND BY NOT ACTIVE----------------");
			groceryItemRepository.findByNotActive().forEach(System.out::println);
			System.out.println();

			System.out.println("----------------COUNT----------------");
			System.out.println("Number of grocery items: " + groceryItemRepository.count());
		};
	}
}
