# mongodb--demo

Spring Boot demo project for MongoDB (NoSQL)

## Getting started

- What is MongoDB? -> https://www.mongodb.com/what-is-mongodb
- _MongoExpress_ is a UI tool to view the data stored in MongoDB
- Run the command below to start MongoDB and MongoExpress containers
```
docker-compose up -d     (-d stands for detached mode)
```
- MongoExpress will be running on _http://localhost:8081_
- The five critical differences between SQL vs NoSQL are
  - SQL databases are relational, NoSQL databases are non-relational.
  - SQL databases use structured query language and have a predefined schema. NoSQL databases have dynamic schemas for unstructured data.
  - SQL databases are vertically scalable, while NoSQL databases are horizontally scalable.
  - SQL databases are table-based, while NoSQL databases are document, key-value, graph, or wide-column stores.
  - SQL databases are better for multi-row transactions, while NoSQL is better for unstructured data like documents or JSON.
- In this demo project We use the MongoRepository interface to store and retrieve data, but You can use MongoTemplate as well.