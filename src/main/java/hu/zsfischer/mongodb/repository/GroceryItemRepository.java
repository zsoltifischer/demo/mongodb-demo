package hu.zsfischer.mongodb.repository;

import hu.zsfischer.mongodb.model.GroceryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * In @Query we use JSON query language
 * <a href="https://docs.spring.io/spring-data/mongodb/docs/1.2.x/reference/html/mongo.repositories.html">...</a>
 */
public interface GroceryItemRepository extends MongoRepository<GroceryItem, String>
{
    @Query("{name:'?0'}")
    GroceryItem findItemByName(String name);

    //This will return only the 'name' and 'quantity' fields (and ID, but it is default), so 'category' will be null in the response
    @Query(value="{ category : ?0 }", fields="{'name' : 1, 'quantity' : 1}")
    List<GroceryItem> findByCategory(String category);

    @Query("{quantity : {$gt : ?0}}")
    List<GroceryItem> findByQuantityGreaterThan(int quantity);

    @Query("{quantity : {$gte : ?0}}")
    List<GroceryItem> findByQuantityGreaterThanEqual(int quantity);

    @Query("{quantity : {$lt : ?0}}")
    List<GroceryItem> findByQuantityLessThan(int quantity);

    @Query("{quantity : {$lte : ?0}}")
    List<GroceryItem> findByQuantityLessThanEqual(int quantity);

    @Query("{quantity : ?0}")
    Page<GroceryItem> findPageByQuantity(int quantity, Pageable pageable);

    @Query("{quantity :  {$gt : ?0, $lt : ?1}}")
    List<GroceryItem> findByQuantityBetween(int from, int to);

    //ne == Not Equals
    @Query("{name :  { $ne : null}}")
    List<GroceryItem> findByNameNotNull();

    @Query("{name : null}")
    List<GroceryItem> findByNameNull();

    @Query("{active : true}")
    List<GroceryItem> findByActive();

    @Query("{active : false}")
    List<GroceryItem> findByNotActive();

    long count();
}
